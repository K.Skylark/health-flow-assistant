from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import login
from django.shortcuts import redirect
from .models import Patient, MedicalEvent, Medication, Prescription
from .forms import AppointmentForm, PatientRegistrationForm

def search_patient(request):
    if request.method == 'POST':
        search_query = request.POST.get('search_query', '')
        patients = Patient.objects.filter(
            first_name__icontains=search_query) | Patient.objects.filter(
            last_name__icontains=search_query)

        return render(request, 'search_results.html', {'patients': patients, 'search_query': search_query})

    return render(request, 'search_patient.html')

def register_patient(request):
    if request.method == 'POST':
        form = PatientRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('patient_dashboard')
    else:
        form = PatientRegistrationForm()
    return render(request, 'registration/register_patient.html', {'form': form})



def patient_dashboard(request):
    # Display patient-specific information
    return render(request, 'patient_dashboard.html')

@login_required
def view_prescriptions(request):
    user = request.user
    prescriptions = Prescription.objects.filter(patient=user.patient)

    return render(request, 'view_prescriptions.html', {'prescriptions': prescriptions})

@login_required
def make_appointment(request):
    # Handle appointment form logic
    if request.method == 'POST':
        form = AppointmentForm(request.POST)
        if form.is_valid():
            # Save appointment
            # ...
            return redirect('patient_dashboard')
    else:
        form = AppointmentForm()

    return render(request, 'make_appointment.html', {'form': form})
def view_medical_history(request, patient_id):
    patient = get_object_or_404(Patient, pk=patient_id)

   
    events = MedicalEvent.objects.filter(patient=patient)

    return render(request, 'medical_history.html', {'patient': patient, 'events': events})


def home(request):
    return render(request, 'home.html')