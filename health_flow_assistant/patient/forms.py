
from django import forms
from .models import Patient, MedicalEvent
from django.contrib.auth.forms import UserCreationForm

class AppointmentForm(forms.ModelForm):
    class Meta:
        model = MedicalEvent
        exclude = ['date'] 

class PatientRegistrationForm(UserCreationForm):
    class Meta:
        model = Patient
        fields = ['email', 'password1', 'password2', 'first_name', 'last_name', 'date_of_birth', 'gender', 'nationality', 'emergency_contact_name', 'emergency_contact_relationship', 'emergency_contact_number', 'insurance', 'contact_number', 'address']