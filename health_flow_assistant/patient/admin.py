from django.contrib import admin
from .models import Patient, Prescription, MedicalEvent, Medication, MedicalHistory

@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'date_of_birth', 'gender', 'nationality', 'insurance', 'contact_number']
    search_fields = ['first_name', 'last_name']

@admin.register(MedicalEvent)
class MedicalEventAdmin(admin.ModelAdmin):
    list_display = ['patient', 'event_type', 'date', 'description', 'treatment']
    search_fields = ['patient__first_name', 'patient__last_name', 'description']

@admin.register(Medication)
class MedicationAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']

@admin.register(Prescription)
class PrescriptionAdmin(admin.ModelAdmin):
    list_display = ('get_medications', 'dose', 'start_date', 'end_date', 'description')
    search_fields = ['medications', 'description']

    def get_medications(self, obj):
        return ", ".join([medication.name for medication in obj.medications.all()])

