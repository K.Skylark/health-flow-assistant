from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission

class Patient(AbstractUser):
    GENDER_CHOICES = [
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    ]
    NATIONALITY_CHOICES = [
        ('US', 'United States'),
        ('CA', 'Canada'),
        ('UK', 'United Kingdom'),
        ('FR', 'France'),
        ('PL', 'Poland'),
    ]
    INSURANCE_STATUS = [
        ('Y', 'Insured'),
        ('N', 'Non-Insured'),
        ('DK', 'No info'),
    ]
    
    # Dodaj pola związane z pacjentem
    first_name = models.CharField(max_length=30)
    middle_name = models.CharField(max_length=40, blank=True)
    mothers_maiden_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=50)
    date_of_birth = models.DateField(blank=True, null=True)
    date_of_death = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default='O')
    nationality = models.CharField(max_length=2, choices=NATIONALITY_CHOICES, blank=True, null=True)
    emergency_contact_name = models.CharField(max_length=100, blank=True)
    emergency_contact_relationship = models.CharField(max_length=100, blank=True)
    emergency_contact_number = models.IntegerField(blank=True, null=True)
    insurance = models.CharField(max_length=2, choices=INSURANCE_STATUS, default='DK')
    created_at = models.DateTimeField(auto_now_add=True)
    contact_number = models.CharField(max_length=15, blank=True)
    email = models.EmailField(blank=True)
    address = models.CharField(max_length=255, blank=True)
    
    # Dodaj pola związane z użytkownikiem (dziedziczone z AbstractUser)
    USERNAME_FIELD = 'email'
    groups = models.ManyToManyField(Group, related_name='patient_users')
    user_permissions = models.ManyToManyField(Permission, related_name='patient_users')
    
    def __str__(self):
        return f"{self.first_name} {self.last_name}"
    
class Medication(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Prescription(models.Model):
    medications = models.ManyToManyField(Medication)
    dose = models.CharField(max_length=125)
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return f"{self.medications} - {self.dose}"



class MedicalEvent(models.Model):
    EVENT_TYPE_CHOICES = [
        ('D', 'Disease'),
        ('S', 'Sickness'),
        ('I', 'Injury'),
        ('W', 'Weight'),
        ('H', 'Height'),
        ('BP', 'Blood Pressure'),
        ('HR', 'Heart Rate'),
    ]

    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    event_type = models.CharField(max_length=2, choices=EVENT_TYPE_CHOICES)
    date = models.DateField()
    description = models.TextField()
    attachments = models.FileField(blank=True)
    prescriptions = models.ManyToManyField(Prescription, related_name='events', blank=True)
    treatment = models.TextField(blank=True)

    def __str__(self):
        return f"{self.get_event_type_display()} for {self.patient}"
    
class MedicalHistory(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    events = models.ManyToManyField(MedicalEvent, blank=True)

    def __str__(self):
        return f"Medical history for {self.patient}"

    
