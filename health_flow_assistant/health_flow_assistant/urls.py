"""
URL configuration for health_flow_assistant project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import LoginView
from patient.views import search_patient, view_medical_history, home, register_patient, patient_dashboard, make_appointment, view_prescriptions, view_medical_history

urlpatterns = [
    path('', home, name='home'),
    path('register/', register_patient, name='register'),
    path('patient-dashboard/', patient_dashboard, name='patient_dashboard'),
    path('make-appointment/', make_appointment, name='make_appointment'),
    path('prescriptions/', view_prescriptions, name='view_prescriptions'),
    path('medical-history/', view_medical_history, name='view_medical_history'),
    path('search/', search_patient, name='search_patient'),
    path('medical_history/<int:patient_id>/', view_medical_history, name='view_medical_history'),

    # Dodaj inne trasy URL dla dodatkowych widoków

    # Dodaj trasę URL dla wbudowanego widoku logowania
    path('login/', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),

    # Dodaj trasę URL dla wbudowanego widoku wylogowywania
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),


    # Trasy dla wbudowanego widoku logowania pacjenta
    path('patient-login/', auth_views.LoginView.as_view(template_name='registration/patient_login.html'), name='patient_login'),

    # Dodaj trasy URL dla aplikacji admina
    path('admin/', admin.site.urls),
    
    # Trasy dla wbudowanego widoku resetowania hasła
    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    # Dodaj inne trasy URL według potrzeb
]

# Dodaj tę linię, aby skorzystać z wbudowanych widoków Django do obsługi resetowania hasła
urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]